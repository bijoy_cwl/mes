package com.wub.medicalequipmentsharing.ui.addmedicine

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.databinding.DataBindingUtil
import com.google.firebase.firestore.FieldValue
import com.wub.medicalequipmentsharing.R
import com.wub.medicalequipmentsharing.databinding.ActivityAddMedicineBinding
import com.wub.medicalequipmentsharing.databinding.ActivitySignUpBinding
import com.wub.medicalequipmentsharing.model.District
import com.wub.medicalequipmentsharing.model.Upazila
import com.wub.medicalequipmentsharing.model.UserInfo
import com.wub.medicalequipmentsharing.ui.BaseActivity
import com.wub.medicalequipmentsharing.ui.home.HomeActivity
import com.wub.medicalequipmentsharing.ui.signin.SignInActivity
import com.wub.medicalequipmentsharing.ui.signup.DistrictsSelectActivity
import com.wub.medicalequipmentsharing.utils.Constants
import com.wub.medicalequipmentsharing.utils.NetworkUtility

class AddMedicineActivity : BaseActivity() {
    lateinit var dataBinding: ActivityAddMedicineBinding
    var district: District? = null
    var upazila: Upazila? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_medicine)
        val users = myDB.collection("medicine")

        val cityId = prefs.getValue(prefs.city)
        val areaId = prefs.getValue(prefs.area)
        if (cityId != "") {
            district = Constants.getDistrictsById(applicationContext, cityId)
        }
        if (areaId != "") {
            upazila = Constants.getUpazilasById(applicationContext, areaId)
        }
        val resultLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
                if (result.resultCode == 100) {
                    // There are no request codes
                    val data: Intent? = result.data
                    // doSomeOperations()
                    val district1 = data?.getSerializableExtra("data") as District
                    if (district != null && (district!!.id != district1.id)) {
                        upazila = null
                        dataBinding.upaziliTV.text = "Select an area"
                        dataBinding.upazilaTitleTV.visibility = View.VISIBLE
                        dataBinding.upazilaLL.visibility = View.VISIBLE
                    } else {
                        dataBinding.upazilaTitleTV.visibility = View.VISIBLE
                        dataBinding.upazilaLL.visibility = View.VISIBLE
                    }
                    district = district1
                    dataBinding.cityTV.text = district!!.name

                } else if (result.resultCode == 101) {
                    val data: Intent? = result.data
                    // doSomeOperations()
                    upazila = data?.getSerializableExtra("data") as Upazila
                    dataBinding.upaziliTV.text = upazila!!.name

                }
            }

        if (district == null) {
            dataBinding.cityTV.text = "Select a city"
            dataBinding.upazilaTitleTV.visibility = View.GONE
            dataBinding.upazilaLL.visibility = View.GONE
        } else {
            dataBinding.cityTV.text = district?.name
        }
        if (upazila == null) {
            dataBinding.upaziliTV.text = "Select an area"
        } else {
            dataBinding.upaziliTV.text = upazila!!.name
            dataBinding.upazilaTitleTV.visibility = View.VISIBLE
            dataBinding.upazilaLL.visibility = View.VISIBLE
        }

        dataBinding.cityTV.setOnClickListener {
            val intent = Intent(this, DistrictsSelectActivity::class.java)
            intent.putExtra("from", 1)
            intent.putExtra("disId", "")
            resultLauncher.launch(intent)
        }
        dataBinding.upazilaLL.setOnClickListener {
            val intent = Intent(this, DistrictsSelectActivity::class.java)
            intent.putExtra("from", 2)
            intent.putExtra("disId", district?.id)
            resultLauncher.launch(intent)
        }
        dataBinding.backIV.setOnClickListener {
            finish()
        }

        dataBinding.signUpButton.setOnClickListener {
            val medicineName = dataBinding.medicineNameET.text.toString().trim()
            val qtn = dataBinding.qtyET.text.toString().trim()
            val address = dataBinding.addressET.text.toString().trim()
            if (medicineName == "") {
                displayToast("Enter Medicine Name")
                dataBinding.medicineNameET.requestFocus()
            } else if (qtn == "") {
                displayToast("Enter Quantity")
                dataBinding.qtyET.requestFocus()
            } else if (district == null) {
                displayToast("Select a city")

            } else if (upazila == null) {
                displayToast("Select an area")
            } else if (address == "") {
                displayToast("Enter address")
                dataBinding.addressET.requestFocus()
            } else {
                if (NetworkUtility.isNetworkAvailable(context)) {
                    loadingView.show()
                    val dd = "${System.currentTimeMillis()}"
                    users.document(dd).set(
                        mapOf(
                            "id" to dd,
                            "medicineName" to medicineName,
                            "qtn" to qtn,
                            "av_qtn" to qtn,
                            "address" to address,
                            "city" to district!!.id,
                            "cityName" to district!!.name,
                            "area" to upazila!!.id,
                            "areaName" to upazila!!.name,
                            "donorName" to prefs.getValue(prefs.name),
                            "donorNumber" to prefs.getValue(prefs.number),
                            "status" to "1",
                        )
                    ).addOnSuccessListener {

                        displayToast("Successful")
                        loadingView.dismiss()

                        finish()
                    }.addOnFailureListener {
                        displayToast("Failed")
                        loadingView.dismiss()
                    }

                } else {
                    displayToast("No Internet connection")
                }

            }
        }


    }
}