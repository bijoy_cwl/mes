package com.wub.medicalequipmentsharing.ui.requestmedicine

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.EditText
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.auth.User
import com.wub.medicalequipmentsharing.R
import com.wub.medicalequipmentsharing.databinding.ActivitySearchMedicineBinding
import com.wub.medicalequipmentsharing.model.Medicine
import com.wub.medicalequipmentsharing.ui.BaseActivity
import com.wub.medicalequipmentsharing.utils.NetworkUtility
import java.util.*
import kotlin.collections.ArrayList


class SearchMedicineActivity : BaseActivity(), RequestMedicineAdapter.RequestMedicineListener {
    val availableArray = ArrayList<Medicine>()
    lateinit var dataBinding: ActivitySearchMedicineBinding
    lateinit var adapter: RequestMedicineAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_search_medicine)
        dataBinding.backIV.setOnClickListener {
            finish()
        }

        getData()

        dataBinding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                //Toast.makeText(MainActivity.this, "SEARCH " + query, Toast.LENGTH_LONG).show();
                searchUsers(query)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                //Toast.makeText(MainActivity.this, "SEARCH " + newText, Toast.LENGTH_LONG).show();
                searchUsers(newText)
                return false
            }
        })

    }

    private fun searchUsers(recherche: String) {
        val results: ArrayList<Medicine> = ArrayList()

        if (recherche.isNotEmpty()) {
            val recherche1 =
                recherche.substring(0, 1).uppercase(Locale.getDefault()) + recherche.substring(1)
                    .lowercase(Locale.getDefault())

            for (user in availableArray) {
                if (user.name.contains(recherche1)) {
                    results.add(user)
                }
            }

        }else{
            results.clear()
        }
        showSearchData(results)
    }

    private fun showSearchData(results: ArrayList<Medicine>) {
        if (results.isEmpty()) {
            dataBinding.listRV.visibility = View.GONE
            dataBinding.qtnTV.visibility = View.VISIBLE
            dataBinding.msgTV.visibility = View.VISIBLE
            dataBinding.msgTV.text = "No available medicine"

        } else {
            dataBinding.qtnTV.visibility = View.VISIBLE
            dataBinding.listRV.visibility = View.VISIBLE
            dataBinding.msgTV.visibility = View.GONE

            adapter = RequestMedicineAdapter(results, this)
            dataBinding.listRV.layoutManager = LinearLayoutManager(context)
            dataBinding.listRV.isNestedScrollingEnabled = false
            dataBinding.listRV.adapter = adapter
        }

        dataBinding.qtnTV.text = "${results.size} medicine found'"

    }

    private fun getData() {
        if (NetworkUtility.isNetworkAvailable(context)) {
            loadingView.show()

            val query = myDB.collection("medicine").whereEqualTo("status", "1")

            query.get()
                .addOnSuccessListener {

                    availableArray.clear()

                    for (doc in it.documents) {
                        val id = doc.getString("id").toString()
                        val medicineName = doc.getString("medicineName").toString()
                        val qtn = doc.getString("qtn").toString()
                        val av_qtn = doc.getString("av_qtn").toString()
                        val address = doc.getString("address").toString()
                        val area = doc.getString("area").toString()
                        val areaName = doc.getString("areaName").toString()
                        val city = doc.getString("city").toString()
                        val cityName = doc.getString("cityName").toString()
                        val donorName = doc.getString("donorName").toString()
                        val donorNumber = doc.getString("donorNumber").toString()
                        val status = doc.getString("status").toString()

                        val medicine = Medicine(
                            id,
                            medicineName,
                            qtn,
                            av_qtn,
                            address,
                            area,
                            areaName,
                            city,
                            cityName,
                            donorName,
                            donorNumber,
                            status
                        )

                        if (donorNumber != prefs.getValue(prefs.number) && av_qtn != "0")
                            availableArray.add(medicine)
                    }


                    loadingView.dismiss()

                }.addOnFailureListener {
                    availableArray.clear();
//                    dataBinding.listRV.visibility = View.GONE
//                    dataBinding.msgTV.visibility = View.VISIBLE
//                    dataBinding.msgTV.text = "Something went wrong"
//                    print(it.message)
                    loadingView.dismiss()
                }

        } else {
            displayToast("No internet connection")
        }
    }


    override fun onRequestMedicineSelect(medicine: Medicine, from: Int) {
        if (from == 1) {
            val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:${medicine.donorNumber}"))
            startActivity(intent)
        } else if (from == 2) {
            val smsIntent = Intent(Intent.ACTION_VIEW)
            smsIntent.type = "vnd.android-dir/mms-sms"
            smsIntent.putExtra("address", medicine.donorNumber)
            smsIntent.putExtra(
                "sms_body",
                "Hello, I need the medicine of ${medicine.name}. Would you please give me this? \n\n\n Name: ${
                    prefs.getValue(prefs.name)
                }\nAddress: ${prefs.getValue(prefs.areaName)}, ${prefs.getValue(prefs.cityName)}"
            )
            startActivity(smsIntent)
        } else if (from == 3) {
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
            builder.setTitle("Enter Quantity")
            val input = EditText(this)

            input.setText("1")
            input.hint = "Enter quantity"
            input.inputType = InputType.TYPE_CLASS_NUMBER
            builder.setView(input)

            builder.setPositiveButton("Send") { dialog, which ->

                val qtn = input.text.toString().trim()
                if (qtn == "") {
                    displayToast("Enter quantity")
                    input.requestFocus()
                } else if (qtn.toInt() != 0 && qtn.toInt() <= medicine.av_qtn.toInt()) {
                    sendRequest(medicine, qtn)
                } else {
                    displayToast("You can take maximum ${medicine.av_qtn}")
                }

            }
            builder.setNegativeButton("Cancel") { dialog, which -> dialog.cancel() }

            builder.show()

        }
    }

    private fun sendRequest(medicine: Medicine, qtn: String) {
        if (NetworkUtility.isNetworkAvailable(context)) {
            loadingView.show()

            val medicineC = myDB.collection("medicine")
            val request = myDB.collection("request")
            val dd = "${System.currentTimeMillis()}"

            val avQtn = medicine.av_qtn.toInt() - qtn.toInt()

            request.document(dd).set(
                mapOf(
                    "id" to dd,
                    "medicineId" to medicine.id,
                    "medicineName" to medicine.name,
                    "qtn" to qtn,
                    "address" to "${medicine.address} , ${medicine.areaName} , ${medicine.cityName}",
                    "recipient" to prefs.getValue(prefs.name),
                    "recipientNumber" to prefs.getValue(prefs.number),
                    "donorNumber" to medicine.donorNumber,
                    "status" to "1"
                )
            ).addOnSuccessListener {

                medicineC.document(medicine.id).update(mapOf("av_qtn" to avQtn.toString()))
                    .addOnSuccessListener {
                        displayToast("Request sent successfully")
                        loadingView.dismiss()
                        finish()
                    }.addOnFailureListener {
                        displayToast("Sorry! Something went wrong.")
                        loadingView.dismiss()
                    }

            }.addOnFailureListener {
                displayToast("Sorry! Something went wrong.")
                loadingView.dismiss()
            }

        } else {
            displayToast("No internet connection")
        }

    }


}