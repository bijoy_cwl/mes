package com.wub.medicalequipmentsharing.model

data class Medicine(
    val id: String,
    val name: String,
    val qtn: String,
    val av_qtn: String,
    val address: String,
    val area: String,
    val areaName: String,
    val city: String,
    val cityName: String,
    val donorName: String,
    val donorNumber: String,
    val status: String
)