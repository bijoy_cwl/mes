package com.wub.medicalequipmentsharing.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.wub.medicalequipmentsharing.R
import com.wub.medicalequipmentsharing.ui.BaseActivity
import com.wub.medicalequipmentsharing.ui.home.HomeActivity
import com.wub.medicalequipmentsharing.ui.signin.SignInActivity

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler(Looper.getMainLooper()).postDelayed({

            if (prefs.getIsLogged()) {
                startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
                finish()
            } else {
                startActivity(Intent(this@SplashActivity, SignInActivity::class.java))
                finish()
            }
        }, 3000)

    }
}