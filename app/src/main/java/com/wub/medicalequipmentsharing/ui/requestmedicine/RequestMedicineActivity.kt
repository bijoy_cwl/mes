package com.wub.medicalequipmentsharing.ui.requestmedicine

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.wub.medicalequipmentsharing.R
import com.wub.medicalequipmentsharing.databinding.ActivityRequestMedicineBinding
import com.wub.medicalequipmentsharing.model.District
import com.wub.medicalequipmentsharing.model.FilterData
import com.wub.medicalequipmentsharing.model.Medicine
import com.wub.medicalequipmentsharing.model.Upazila
import com.wub.medicalequipmentsharing.ui.BaseActivity
import com.wub.medicalequipmentsharing.ui.signup.DistrictsSelectActivity
import com.wub.medicalequipmentsharing.utils.Constants
import com.wub.medicalequipmentsharing.utils.NetworkUtility


class RequestMedicineActivity : BaseActivity(), RequestMedicineAdapter.RequestMedicineListener {
    val availableArray = ArrayList<Medicine>()
    lateinit var dataBinding: ActivityRequestMedicineBinding
    lateinit var adapter: RequestMedicineAdapter

    //    var city = ""
//    var cityName = ""
//    var area = ""
//    var areaName = ""
    var district: District? = null
    var upazila: Upazila? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_request_medicine)
        dataBinding.backIV.setOnClickListener {
            finish()
        }
        dataBinding.searchIV.setOnClickListener {
            startActivity(Intent(this, SearchMedicineActivity::class.java))
        }

//        city = prefs.getValue(prefs.city)
//        area = prefs.getValue(prefs.area)
//        cityName = prefs.getValue(prefs.cityName)
//        areaName = prefs.getValue(prefs.areaName)

        val cityId = prefs.getValue(prefs.city)
        val areaId = prefs.getValue(prefs.area)
        if (cityId != "") {
            district = Constants.getDistrictsById(applicationContext, cityId)
        }
        if (areaId != "") {
            upazila = Constants.getUpazilasById(applicationContext, areaId)
        }
        getData()

        val resultLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
                if (result.resultCode == 105) {
                    // There are no request codes
                    val data: Intent? = result.data
                    // doSomeOperations()
                    val filterData = data?.getSerializableExtra("data") as FilterData

                    district = filterData.district
                    upazila = filterData.upazila
                    getData()
                }
            }


        dataBinding.changeLocButton.setOnClickListener {

            val intent = Intent(this, FilterActivity::class.java)
            resultLauncher.launch(intent)
        }


    }

    private fun getData() {
        if (NetworkUtility.isNetworkAvailable(context)) {
            loadingView.show()

            var query = myDB.collection("medicine").whereEqualTo("status", "1")
            query = query.whereEqualTo("city", district?.id)
            query = query.whereEqualTo("area", upazila?.id)

            query.get()
                .addOnSuccessListener {

                    availableArray.clear()

                    for (doc in it.documents) {
                        val id = doc.getString("id").toString()
                        val medicineName = doc.getString("medicineName").toString()
                        val qtn = doc.getString("qtn").toString()
                        val av_qtn = doc.getString("av_qtn").toString()
                        val address = doc.getString("address").toString()
                        val area = doc.getString("area").toString()
                        val areaName = doc.getString("areaName").toString()
                        val city = doc.getString("city").toString()
                        val cityName = doc.getString("cityName").toString()
                        val donorName = doc.getString("donorName").toString()
                        val donorNumber = doc.getString("donorNumber").toString()
                        val status = doc.getString("status").toString()

                        val medicine = Medicine(
                            id,
                            medicineName,
                            qtn,
                            av_qtn,
                            address,
                            area,
                            areaName,
                            city,
                            cityName,
                            donorName,
                            donorNumber,
                            status
                        )

                        if (donorNumber != prefs.getValue(prefs.number) && av_qtn != "0")
                            availableArray.add(medicine)
                    }

                    if (availableArray.isEmpty()) {
                        dataBinding.listRV.visibility = View.GONE
                        dataBinding.qtnTV.visibility = View.VISIBLE
                        dataBinding.msgTV.visibility = View.VISIBLE
                        dataBinding.msgTV.text = "No available medicine"

                    } else {
                        dataBinding.qtnTV.visibility = View.VISIBLE
                        dataBinding.listRV.visibility = View.VISIBLE
                        dataBinding.msgTV.visibility = View.GONE

                        adapter = RequestMedicineAdapter(availableArray, this)
                        dataBinding.listRV.layoutManager = LinearLayoutManager(context)
                        dataBinding.listRV.isNestedScrollingEnabled = false
                        dataBinding.listRV.adapter = adapter
                    }
                    dataBinding.changeLocButton.visibility = View.VISIBLE
                    dataBinding.qtnTV.text =
                        "${availableArray.size} medicine found near '${upazila?.name},${district?.name}'"

                    loadingView.dismiss()

                }.addOnFailureListener {
                    dataBinding.listRV.visibility = View.GONE
                    dataBinding.msgTV.visibility = View.VISIBLE
                    dataBinding.msgTV.text = "Something went wrong"
                    print(it.message)
                    loadingView.dismiss()
                }

        } else {
            displayToast("No internet connection")
        }
    }

    fun popUpWindow() {
        val dialog = Dialog(this@RequestMedicineActivity)
        dialog.setContentView(R.layout.filter_layout)
        val upaziliTV = dialog.findViewById<TextView>(R.id.upaziliTV)
        val upazilaTitleTV = dialog.findViewById<TextView>(R.id.upazilaTitleTV)
        val upazilaLL = dialog.findViewById<LinearLayout>(R.id.upazilaLL)
        val cityTV = dialog.findViewById<TextView>(R.id.cityTV)
        val submitButton = dialog.findViewById<TextView>(R.id.submitButton)
        val cancelButton = dialog.findViewById<TextView>(R.id.cancelButton)


        val resultLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
                if (result.resultCode == 100) {
                    // There are no request codes
                    val data: Intent? = result.data
                    // doSomeOperations()
                    val district1 = data?.getSerializableExtra("data") as District
                    if (district != null && (district!!.id != district1.id)) {
                        upazila = null
                        upaziliTV.text = "Select an area"
                        upazilaTitleTV.visibility = View.VISIBLE
                        upazilaLL.visibility = View.VISIBLE
                    } else {
                        upazilaTitleTV.visibility = View.VISIBLE
                        upazilaLL.visibility = View.VISIBLE
                    }
                    district = district1
                    cityTV.text = district!!.name

                } else if (result.resultCode == 101) {
                    val data: Intent? = result.data
                    // doSomeOperations()
                    upazila = data?.getSerializableExtra("data") as Upazila
                    upaziliTV.text = upazila!!.name

                }
            }

        if (district == null) {
            cityTV.text = "Select a city"
            upazilaTitleTV.visibility = View.GONE
            upazilaLL.visibility = View.GONE
        } else {
            cityTV.text = district?.name
        }
        if (upazila == null) {
            upaziliTV.text = "Select an area"
        } else {
            upaziliTV.text = upazila!!.name
            upazilaTitleTV.visibility = View.VISIBLE
            upazilaLL.visibility = View.VISIBLE
        }

        cityTV.setOnClickListener {
            val intent = Intent(this, DistrictsSelectActivity::class.java)
            intent.putExtra("from", 1)
            intent.putExtra("disId", "")
            resultLauncher.launch(intent)
        }
        upazilaLL.setOnClickListener {
            val intent = Intent(this, DistrictsSelectActivity::class.java)
            intent.putExtra("from", 2)
            intent.putExtra("disId", district?.id)
            resultLauncher.launch(intent)
        }

        cancelButton.setOnClickListener {
            dialog.dismiss()
        }
        submitButton.setOnClickListener {
            dialog.dismiss()
            getData()
        }

        dialog.setTitle("Select Location")
        dialog.show()
    }

    override fun onRequestMedicineSelect(medicine: Medicine, from: Int) {
        if (from == 1) {
            val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:${medicine.donorNumber}"))
            startActivity(intent)
        } else if (from == 2) {
            val smsIntent = Intent(Intent.ACTION_VIEW)
            smsIntent.type = "vnd.android-dir/mms-sms"
            smsIntent.putExtra("address", medicine.donorNumber)
            smsIntent.putExtra(
                "sms_body",
                "Hello, I need the medicine of ${medicine.name}. Would you please give me this? \n\n\n Name: ${
                    prefs.getValue(prefs.name)
                }\nAddress: ${prefs.getValue(prefs.areaName)}, ${prefs.getValue(prefs.cityName)}"
            )
            startActivity(smsIntent)
        } else if (from == 3) {
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
            builder.setTitle("Enter Quantity")
            val input = EditText(this)

            input.setText("1")
            input.hint = "Enter quantity"
            input.inputType = InputType.TYPE_CLASS_NUMBER
            builder.setView(input)

            builder.setPositiveButton("Send") { dialog, which ->

                val qtn = input.text.toString().trim()
                if (qtn == "") {
                    displayToast("Enter quantity")
                    input.requestFocus()
                } else if (qtn.toInt() != 0 && qtn.toInt() <= medicine.av_qtn.toInt()) {
                    sendRequest(medicine, qtn)
                } else {
                    displayToast("You can take maximum ${medicine.av_qtn}")
                }

            }
            builder.setNegativeButton("Cancel") { dialog, which -> dialog.cancel() }

            builder.show()

        }
    }

    private fun sendRequest(medicine: Medicine, qtn: String) {
        if (NetworkUtility.isNetworkAvailable(context)) {
            loadingView.show()

            val medicineC = myDB.collection("medicine")
            val request = myDB.collection("request")
            val dd = "${System.currentTimeMillis()}"

            val avQtn = medicine.av_qtn.toInt() - qtn.toInt()

            request.document(dd).set(
                mapOf(
                    "id" to dd,
                    "medicineId" to medicine.id,
                    "medicineName" to medicine.name,
                    "qtn" to qtn,
                    "address" to "${medicine.address} , ${medicine.areaName} , ${medicine.cityName}",
                    "recipient" to prefs.getValue(prefs.name),
                    "recipientNumber" to prefs.getValue(prefs.number),
                    "donorNumber" to medicine.donorNumber,
                    "status" to "1"
                )
            ).addOnSuccessListener {

                medicineC.document(medicine.id).update(mapOf("av_qtn" to avQtn.toString()))
                    .addOnSuccessListener {
                        displayToast("Request sent successfully")
                        loadingView.dismiss()
                        finish()
                    }.addOnFailureListener {
                        displayToast("Sorry! Something went wrong.")
                        loadingView.dismiss()
                    }

            }.addOnFailureListener {
                displayToast("Sorry! Something went wrong.")
                loadingView.dismiss()
            }

        } else {
            displayToast("No internet connection")
        }

    }


}