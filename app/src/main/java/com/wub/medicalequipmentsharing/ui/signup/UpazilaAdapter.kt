package com.wub.medicalequipmentsharing.ui.signup

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.wub.medicalequipmentsharing.R
import com.wub.medicalequipmentsharing.model.District
import com.wub.medicalequipmentsharing.model.Upazila

class UpazilaAdapter(
    private var list: List<Upazila>,
    private val upazilaListener: UpazilaListener
) :
    RecyclerView.Adapter<UpazilaAdapter.UpazilaHolder>() {

    inner class UpazilaHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.titleTV)
    }

    fun setFilter(iDayses: List<Upazila>) {
        list = ArrayList()
        (list as ArrayList<Upazila>).clear()
        (list as ArrayList<Upazila>).addAll(iDayses)
        notifyDataSetChanged()
    }

    public interface UpazilaListener {
        fun onUpazilaSelect(upazila: Upazila)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UpazilaHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.district_item, parent, false);
        return UpazilaHolder(view)
    }

    override fun onBindViewHolder(holder: UpazilaHolder, position: Int) {
        val item = list[holder.adapterPosition]
        holder.title.text = item.name

        holder.itemView.setOnClickListener {
            upazilaListener.onUpazilaSelect(item)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}