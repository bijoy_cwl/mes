package com.wub.medicalequipmentsharing.ui.signup

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.wub.medicalequipmentsharing.R
import com.wub.medicalequipmentsharing.databinding.ActivityDistrictsSelectBinding
import com.wub.medicalequipmentsharing.databinding.ActivitySignUpBinding
import com.wub.medicalequipmentsharing.model.District
import com.wub.medicalequipmentsharing.model.Upazila
import com.wub.medicalequipmentsharing.ui.BaseActivity
import com.wub.medicalequipmentsharing.utils.Constants
import java.util.*
import kotlin.collections.ArrayList

class DistrictsSelectActivity : BaseActivity(), DistrictAdapter.DistrictListener,
    UpazilaAdapter.UpazilaListener,
    SearchView.OnQueryTextListener {

    lateinit var dataBinding: ActivityDistrictsSelectBinding

    //  var searchView: SearchView? = null
    private var districtList: List<District> = ArrayList()
    private var upazilaList: List<Upazila> = ArrayList()
    lateinit var adapter: DistrictAdapter
    lateinit var upazilaAdapter: UpazilaAdapter

    var from = 1
    var disId = ""


    //    companion object {
//
//        internal const val EXTRAS_FROM = "from"
//
//
//        fun getStartIntent(context: Context, from: Int) {
//            val intent = Intent(context, CityListActivity::class.java)
//            intent.putExtra(EXTRAS_FROM, from)
//            context.startActivity(intent)
//        }
//    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_districts_select)

        setSupportActionBar(dataBinding.toolbarMain)

        from = intent.getIntExtra("from", 1)
        disId = intent.getStringExtra("disId").toString()

        if (from == 1){
            dataBinding.titleTV.text = "Select city"
        }else{
            dataBinding.titleTV.text = "Select an area"
        }

        adapter = DistrictAdapter(districtList, this)
        upazilaAdapter = UpazilaAdapter(upazilaList, this)

        if (from == 1) {

            districtList = Constants.getDistricts(applicationContext)
            adapter = DistrictAdapter(districtList, this)
            dataBinding.districtsRV.layoutManager = LinearLayoutManager(this)
            dataBinding.districtsRV.adapter = adapter

        } else {
            Log.e("dis id",disId)
            upazilaList = Constants.getUpazilas(applicationContext, disId)
            upazilaAdapter = UpazilaAdapter(upazilaList!!, this)
            dataBinding.districtsRV.layoutManager = LinearLayoutManager(this)
            dataBinding.districtsRV.adapter = upazilaAdapter
        }
        //  searchView!!.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        dataBinding.serachView.setOnQueryTextListener(this)

        dataBinding.backIV.setOnClickListener {
            finish()
        }

    }

    override fun onDistrictSelect(district: District) {
        val intent = Intent();
        intent.putExtra("data", district)
        setResult(100, intent);
        finish();
    }

//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        val menuInflater = menuInflater
//        menuInflater.inflate(R.menu.search_menu, menu)
//        val searchManager =
//            context.getSystemService(Context.SEARCH_SERVICE) as SearchManager
//        val searchMenuItem = menu!!.findItem(R.id.search)
//        searchView = searchMenuItem.actionView as SearchView?
//
//        return true
//    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        dataBinding.serachView.clearFocus()
        if (from == 1)
            adapter.setFilter(districtList!!)
        else
            upazilaAdapter.setFilter(upazilaList!!)

        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if (from == 1) {
            if (newText != "") {
                val searchText = newText?.lowercase(Locale.getDefault())
                val newList = ArrayList<District>()

                for (dList in districtList!!) {
                    val title = dList.name.lowercase(Locale.getDefault())
                    if (title.contains(searchText.toString())) {
                        newList.add(dList)
                    }
                }
                adapter.setFilter(newList)

            } else {
                adapter.setFilter(districtList!!)
            }

        } else {
            if (newText != "") {
                val searchText = newText?.lowercase(Locale.getDefault())
                val newList = ArrayList<Upazila>()

                for (dList in upazilaList!!) {
                    val title = dList.name.lowercase(Locale.getDefault())
                    if (title.contains(searchText.toString())) {
                        newList.add(dList)
                    }
                }
                upazilaAdapter.setFilter(newList)

            } else {
                upazilaAdapter.setFilter(upazilaList!!)
            }

        }
        return true
    }

    override fun onUpazilaSelect(upazila: Upazila) {
        val intent = Intent();
        intent.putExtra("data", upazila)
        setResult(101, intent);
        finish();
    }
}