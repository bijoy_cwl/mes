package com.wub.medicalequipmentsharing

import android.app.Application

class BaseApp : Application() {
    override fun onCreate() {
        super.onCreate()
        HttpsTrustManager().allowAllSSL()
    }
}