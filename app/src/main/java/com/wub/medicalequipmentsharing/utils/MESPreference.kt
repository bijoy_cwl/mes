package com.wub.medicalequipmentsharing.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import com.wub.medicalequipmentsharing.model.UserInfo

class MESPreference(context: Context) {
    val shName = "mes"
    val name = "name"
    val number = "number"
    val email = "email"
    val city = "city"
    val cityName = "cityName"
    val area = "area"
    val areaName = "areaName"
    val type = "type"
    val builderId = "builderId"
    val authorityId = "authorityId"
    val occupation = "occupation"

    val dob = "dob"
    val isLogged = "isLogged"

    var sharedPreferences = context.getSharedPreferences(shName, Context.MODE_PRIVATE)
    var editor: SharedPreferences.Editor? = null

    @SuppressLint("CommitPrefEdits")
    fun setUser(user: UserInfo, logged: Boolean) {
        editor = sharedPreferences.edit()
        editor!!.putString(name, user.name)
        editor!!.putString(number, user.number)
        editor!!.putString(email, user.email)
        editor!!.putString(city, user.city)
        editor!!.putString(cityName, user.cityName)
        editor!!.putString(area, user.area)
        editor!!.putString(areaName, user.areaName)
        editor!!.putBoolean(isLogged, logged)
        editor!!.commit()
        editor!!.apply()
    }

    @SuppressLint("CommitPrefEdits")
    fun setIsLogged(logged: Boolean) {
        editor = sharedPreferences.edit()
        editor!!.putBoolean(isLogged, logged)
        editor!!.commit()
        editor!!.apply()
    }

    fun getIsLogged(): Boolean {
        return sharedPreferences.getBoolean(isLogged, false)
    }

    fun getValue(key: String): String {
        return sharedPreferences.getString(key, "").toString()
    }

}