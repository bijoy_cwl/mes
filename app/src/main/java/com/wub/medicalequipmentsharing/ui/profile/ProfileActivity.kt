package com.wub.medicalequipmentsharing.ui.profile

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.wub.medicalequipmentsharing.R
import com.wub.medicalequipmentsharing.databinding.ActivityProfileBinding
import com.wub.medicalequipmentsharing.ui.BaseActivity
import com.wub.medicalequipmentsharing.ui.orders.OrdersActivity
import com.wub.medicalequipmentsharing.ui.signin.SignInActivity

class ProfileActivity : BaseActivity() {
    lateinit var dataBinding: ActivityProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_profile)
        dataBinding.backIV.setOnClickListener {
            finish()
        }

        dataBinding.nameTV.text = prefs.getValue(prefs.name)
        dataBinding.emailTV.text = "Email: " + prefs.getValue(prefs.email)
        dataBinding.numberTV.text = "Mobile: " + prefs.getValue(prefs.number)
        dataBinding.areaTV.text = "Area: " + prefs.getValue(prefs.areaName)
        dataBinding.cityTV.text = "City: " + prefs.getValue(prefs.cityName)


        dataBinding.ordersButton.setOnClickListener {
            val intent = Intent(this, OrdersActivity::class.java)
            intent.putExtra("from",1)
            startActivity(intent)
        }

        dataBinding.myShareButton.setOnClickListener {
            val intent = Intent(this, OrdersActivity::class.java)
            intent.putExtra("from",2)
            startActivity(intent)
        }
        dataBinding.logoutButton.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            //set title for alert dialog
            builder.setTitle(getString(R.string.app_name))
            //set message for alert dialog
            builder.setMessage(getString(R.string.do_you_want_to_logout))
            //builder.setIcon()

            //performing positive action
            builder.setPositiveButton("Yes") { dialogInterface, which ->
                val sharedPreferences =
                    this.getSharedPreferences(prefs.shName, Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                val email = sharedPreferences.getString(prefs.number, "")
                editor!!.clear()
                editor.apply()
                editor.commit()
                editor.putString(prefs.number, email)
                editor.apply()
                editor.commit()

                val intent = Intent(this, SignInActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                finish()
            }

            //performing negative action
            builder.setNegativeButton("No") { dialogInterface, which ->
                dialogInterface.dismiss()
            }
            // Create the AlertDialog
            val alertDialog: AlertDialog = builder.create()
            // Set other dialog properties
            alertDialog.setCancelable(false)
            alertDialog.show()
        }
    }
}