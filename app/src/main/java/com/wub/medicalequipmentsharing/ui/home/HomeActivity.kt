package com.wub.medicalequipmentsharing.ui.home

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.wub.medicalequipmentsharing.R
import com.wub.medicalequipmentsharing.databinding.ActivityAddMedicineBinding
import com.wub.medicalequipmentsharing.databinding.ActivityHomeBinding
import com.wub.medicalequipmentsharing.ui.BaseActivity
import com.wub.medicalequipmentsharing.ui.aboutus.AboutUsActivity
import com.wub.medicalequipmentsharing.ui.addmedicine.AddMedicineActivity
import com.wub.medicalequipmentsharing.ui.myrequest.MyRequestActivity
import com.wub.medicalequipmentsharing.ui.profile.ProfileActivity
import com.wub.medicalequipmentsharing.ui.requestmedicine.RequestMedicineActivity

class HomeActivity : BaseActivity() {

    lateinit var dataBinding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_home)

        dataBinding.donateMedicineLL.setOnClickListener {
            startActivity(Intent(this, AddMedicineActivity::class.java))
        }
        dataBinding.receiveMedicineLL.setOnClickListener {
            startActivity(Intent(this, RequestMedicineActivity::class.java))
        }
        dataBinding.receiptRequestLL.setOnClickListener {
            startActivity(Intent(this, MyRequestActivity::class.java))
        }
        dataBinding.profileLL.setOnClickListener {
            startActivity(Intent(this, ProfileActivity::class.java))
        }

        dataBinding.aboutUsTV.setOnClickListener {
            startActivity(Intent(this, AboutUsActivity::class.java))
        }

    }
}