package com.wub.medicalequipmentsharing.model

import java.io.Serializable

data class District(val id: String, val division_id: String, val name: String): Serializable