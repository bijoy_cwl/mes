package com.wub.medicalequipmentsharing.ui.signin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import com.wub.medicalequipmentsharing.R
import com.wub.medicalequipmentsharing.databinding.ActivitySignInBinding
import com.wub.medicalequipmentsharing.databinding.ActivitySignUpBinding
import com.wub.medicalequipmentsharing.model.UserInfo
import com.wub.medicalequipmentsharing.ui.BaseActivity
import com.wub.medicalequipmentsharing.ui.home.HomeActivity
import com.wub.medicalequipmentsharing.ui.signup.SignUpActivity
import com.wub.medicalequipmentsharing.utils.Constants.md5
import com.wub.medicalequipmentsharing.utils.NetworkUtility

class SignInActivity : BaseActivity() {

    lateinit var dataBinding: ActivitySignInBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in)
        val users = myDB.collection("users")

        dataBinding.numberET.setText(prefs.getValue(prefs.number))

        dataBinding.signUpHereTV.setOnClickListener {
            startActivity(Intent(this, SignUpActivity::class.java))
            finish()
        }
        dataBinding.signInButton.setOnClickListener {
            val mobile = dataBinding.numberET.text.toString().trim()
            val password1 = dataBinding.passwordET.text.toString().trim()

            if (mobile == "") {
                displayToast("Enter mobile number")
                dataBinding.numberET.requestFocus()
            } else if (password1 == "") {
                displayToast("Enter password")
                dataBinding.passwordET.requestFocus()
            } else {
                if (NetworkUtility.isNetworkAvailable(context)) {
                    loadingView.show()


                    users.whereEqualTo("number", mobile).get()
                        .addOnCompleteListener {
                            if (it.result?.documents?.size!! > 0) {
                                loadingView.dismiss()
                                // displayToast("User Exists")

                                for (doc in it.result!!.documents) {

                                    val name: String =
                                        doc?.getString("name").toString()
                                    val number: String =
                                        doc?.getString("number").toString()
                                    val password: String =
                                        doc?.getString("password").toString()
                                    val email: String =
                                        doc?.getString("email").toString()
                                    val city: String =
                                        doc?.getString("city").toString()
                                    val cityName: String =
                                        doc?.getString("cityName").toString()
                                    val area: String =
                                        doc?.getString("area").toString()
                                    val areaName: String =
                                        doc?.getString("areaName").toString()


                                    if (md5(password1) == password) {
                                        val userInfo = UserInfo(
                                            name,
                                            number,
                                            email,
                                            city,
                                            cityName,
                                            area,
                                            areaName
                                        )
                                        prefs.setUser(userInfo, true)
                                        loadingView.dismiss()
                                        displayToast("Login Successful")
                                        startActivity(
                                            Intent(
                                                this,
                                                HomeActivity::class.java
                                            )
                                        )
                                        finish()
                                    } else {
                                        loadingView.dismiss()
                                        displayToast("Invalid password")
                                    }


                                }

                            } else {
                                loadingView.dismiss()
                                displayToast("User not exists")
                            }
                        }

                } else {
                    displayToast("No Internet connection")
                }
            }
        }
    }
}