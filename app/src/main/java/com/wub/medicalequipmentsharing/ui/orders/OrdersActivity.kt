package com.wub.medicalequipmentsharing.ui.orders

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.Query
import com.wub.medicalequipmentsharing.R
import com.wub.medicalequipmentsharing.databinding.ActivityOrdersBinding
import com.wub.medicalequipmentsharing.databinding.ActivityProfileBinding
import com.wub.medicalequipmentsharing.model.RecipientMedicine
import com.wub.medicalequipmentsharing.ui.BaseActivity
import com.wub.medicalequipmentsharing.ui.myrequest.RecipientRequestMedicineAdapter
import com.wub.medicalequipmentsharing.utils.NetworkUtility

class OrdersActivity : BaseActivity(), OrdersAdapter.OrdersListener {

    lateinit var dataBinding: ActivityOrdersBinding
    val availableArray = ArrayList<RecipientMedicine>()
    lateinit var adapter: OrdersAdapter
    var from = 1
    var query: Query? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_orders)

        from = intent.getIntExtra("from", 1)

        if(from == 1){
            dataBinding.titleTV.text = "My Requests"
        }else{
            dataBinding.titleTV.text = "My Shares"
        }
        dataBinding.backIV.setOnClickListener {
            finish()
        }

        getData()
    }

    private fun getData() {
        if (NetworkUtility.isNetworkAvailable(context)) {
            loadingView.show()

            query = if (from == 1) {
                myDB.collection("request").whereEqualTo("recipientNumber", prefs.getValue(prefs.number))
            }else{
                myDB.collection("request").whereEqualTo("donorNumber", prefs.getValue(prefs.number))
            }

            query!!.get()
                .addOnSuccessListener {

                    availableArray.clear()

                    for (doc in it.documents) {
                        val id = doc.getString("id").toString()
                        val medicineId = doc.getString("medicineId").toString()
                        val medicineName = doc.getString("medicineName").toString()
                        val qtn = doc.getString("qtn").toString()
                        val address = doc.getString("address").toString()
                        val recipient = doc.getString("recipient").toString()
                        val recipientNumber = doc.getString("recipientNumber").toString()
                        val donorNumber = doc.getString("donorNumber").toString()
                        val status = doc.getString("status").toString()

                        val medicine = RecipientMedicine(
                            id,
                            medicineId,
                            medicineName,
                            qtn,
                            address,
                            recipient,
                            recipientNumber,
                            donorNumber,
                            status
                        )

                        availableArray.add(medicine)
                    }

                    if (availableArray.isEmpty()) {
                        dataBinding.listRV.visibility = View.GONE
                        dataBinding.msgTV.visibility = View.VISIBLE
                        dataBinding.msgTV.text = "No item found"
                    } else {
                        dataBinding.listRV.visibility = View.VISIBLE
                        dataBinding.msgTV.visibility = View.GONE

                        adapter = OrdersAdapter(from,availableArray, this)
                        dataBinding.listRV.layoutManager = LinearLayoutManager(context)
                        dataBinding.listRV.isNestedScrollingEnabled = false
                        dataBinding.listRV.adapter = adapter
                    }
                    loadingView.dismiss()

                }.addOnFailureListener {
                    dataBinding.listRV.visibility = View.GONE
                    dataBinding.msgTV.visibility = View.VISIBLE
                    dataBinding.msgTV.text = "Something went wrong"

                    print(it.message)
                    loadingView.dismiss()
                }

        } else {
            displayToast("No internet connection")
        }

    }

    override fun onOrdersSelect(recipientMedicine: RecipientMedicine, from: Int) {
        if (from == 2) {
            if (NetworkUtility.isNetworkAvailable(context)) {
                val medicineC = myDB.collection("medicine")
                val request = myDB.collection("request")
                medicineC.whereEqualTo("id", recipientMedicine.medicineId).limit(1)
                medicineC.get().addOnSuccessListener {
                    val docSP = it.documents
                    for (doc in docSP) {
                        if (doc.getString("id") == recipientMedicine.medicineId) {
                            val av_qtn = doc.getString("av_qtn")?.toInt()
                            val name = doc.getString("medicineName")
                            Log.e("name ", " $name")
                            val rQtn = recipientMedicine.qtn.toInt()
                            Log.e("avQtn ", " $av_qtn")
                            Log.e("rQtn ", " $rQtn")

                            val fQtn = av_qtn?.plus(rQtn)
                            Log.e("fQtn ", " $fQtn")
                            medicineC.document(recipientMedicine.medicineId)
                                .update(mapOf("av_qtn" to fQtn.toString()))
                            request.document(recipientMedicine.id).delete()
                            displayToast("Canceled")
                            loadingView.dismiss()
                            getData()
                            break
                        }

                    }
                }.addOnFailureListener {
                    displayToast("Sorry! Something went wrong.1")
                    loadingView.dismiss()
                    getData()
                }
            } else {
                displayToast("No internet connection")
            }
        }
    }

}