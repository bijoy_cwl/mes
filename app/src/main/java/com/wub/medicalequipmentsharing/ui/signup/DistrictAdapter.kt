package com.wub.medicalequipmentsharing.ui.signup

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.wub.medicalequipmentsharing.R
import com.wub.medicalequipmentsharing.model.District

class DistrictAdapter(
    private var list: List<District>,
    private val districtListener: DistrictListener
) :
    RecyclerView.Adapter<DistrictAdapter.DistrictHolder>() {

    inner class DistrictHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.titleTV)
    }

    fun setFilter(iDayses: List<District>) {
        list = ArrayList()
        (list as ArrayList<District>).clear()
        (list as ArrayList<District>).addAll(iDayses)
        notifyDataSetChanged()
    }

    public interface DistrictListener {
        fun onDistrictSelect(district: District)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DistrictHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.district_item, parent, false);
        return DistrictHolder(view)
    }

    override fun onBindViewHolder(holder: DistrictHolder, position: Int) {
        val item = list[holder.adapterPosition]
        holder.title.text = item.name

        holder.itemView.setOnClickListener {
            districtListener.onDistrictSelect(item)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}