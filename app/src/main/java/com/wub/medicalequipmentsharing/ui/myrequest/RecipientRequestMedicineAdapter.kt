package com.wub.medicalequipmentsharing.ui.myrequest

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.wub.medicalequipmentsharing.R

import com.wub.medicalequipmentsharing.model.Medicine
import com.wub.medicalequipmentsharing.model.RecipientMedicine

class RecipientRequestMedicineAdapter(
    private var list: List<RecipientMedicine>,
    private val recipientRequestMedicineListener: RecipientRequestMedicineListener
) :
    RecyclerView.Adapter<RecipientRequestMedicineAdapter.RecipientRequestMedicineHolder>() {

    inner class RecipientRequestMedicineHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val medicineNameTV: TextView = itemView.findViewById(R.id.medicineNameTV)
        val qtyTV: TextView = itemView.findViewById(R.id.qtyTV)
        val nameTV: TextView = itemView.findViewById(R.id.nameTV)
        val addressTV: TextView = itemView.findViewById(R.id.addressTV)
        val rejectButton: Button = itemView.findViewById(R.id.rejectButton)
        val acceptButton: Button = itemView.findViewById(R.id.acceptButton)
        val callButton: Button = itemView.findViewById(R.id.callButton)
        val messageButton: Button = itemView.findViewById(R.id.messageButton)
    }

//    fun setFilter(iDayses: List<District>) {
//        list = ArrayList()
//        (list as ArrayList<District>).clear()
//        (list as ArrayList<District>).addAll(iDayses)
//        notifyDataSetChanged()
//    }

    public interface RecipientRequestMedicineListener {
        fun onRequestMedicineSelect(recipientMedicine: RecipientMedicine,from: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipientRequestMedicineHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.recipient_medicine_item_layout, parent, false);
        return RecipientRequestMedicineHolder(view)
    }

    override fun onBindViewHolder(holder: RecipientRequestMedicineHolder, position: Int) {
        val item = list[holder.adapterPosition]
        holder.medicineNameTV.text = "Medicine Name: ${item.medicineName}"
        holder.qtyTV.text = "Quantity: ${item.qtn}"
        holder.addressTV.text = "Address: ${item.address}"
        holder.nameTV.text = "Recipient Name: ${item.recipient}"

        holder.acceptButton.setOnClickListener {
            recipientRequestMedicineListener.onRequestMedicineSelect(item,1)
        }
        holder.rejectButton.setOnClickListener {
            recipientRequestMedicineListener.onRequestMedicineSelect(item,2)
        }
        holder.callButton.setOnClickListener {
            recipientRequestMedicineListener.onRequestMedicineSelect(item,3)
        }
        holder.messageButton.setOnClickListener {
            recipientRequestMedicineListener.onRequestMedicineSelect(item,4)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}