package com.wub.medicalequipmentsharing.ui.myrequest

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.wub.medicalequipmentsharing.R
import com.wub.medicalequipmentsharing.databinding.ActivityMyRequestBinding
import com.wub.medicalequipmentsharing.databinding.ActivityRequestMedicineBinding
import com.wub.medicalequipmentsharing.model.Medicine
import com.wub.medicalequipmentsharing.model.RecipientMedicine
import com.wub.medicalequipmentsharing.ui.BaseActivity
import com.wub.medicalequipmentsharing.ui.requestmedicine.RequestMedicineAdapter
import com.wub.medicalequipmentsharing.utils.NetworkUtility

class MyRequestActivity : BaseActivity(),
    RecipientRequestMedicineAdapter.RecipientRequestMedicineListener {

    val availableArray = ArrayList<RecipientMedicine>()
    lateinit var dataBinding: ActivityMyRequestBinding
    lateinit var adapter: RecipientRequestMedicineAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_request)

        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_request)
        dataBinding.backIV.setOnClickListener {
            finish()
        }
        getData()
    }

    private fun getData() {
        if (NetworkUtility.isNetworkAvailable(context)) {
            loadingView.show()

            var query = myDB.collection("request").whereEqualTo("status", "1")
            query = query.whereEqualTo("donorNumber", prefs.getValue(prefs.number))

            query.get()
                .addOnSuccessListener {

                    availableArray.clear()

                    for (doc in it.documents) {
                        val id = doc.getString("id").toString()
                        val medicineId = doc.getString("medicineId").toString()
                        val medicineName = doc.getString("medicineName").toString()
                        val qtn = doc.getString("qtn").toString()
                        val address = doc.getString("address").toString()
                        val recipient = doc.getString("recipient").toString()
                        val recipientNumber = doc.getString("recipientNumber").toString()
                        val donorNumber = doc.getString("donorNumber").toString()
                        val status = doc.getString("status").toString()

                        val medicine = RecipientMedicine(
                            id,
                            medicineId,
                            medicineName,
                            qtn,
                            address,
                            recipient,
                            recipientNumber,
                            donorNumber,
                            status
                        )

                        availableArray.add(medicine)
                    }

                    if (availableArray.isEmpty()) {
                        dataBinding.listRV.visibility = View.GONE
                        dataBinding.msgTV.visibility = View.VISIBLE
                        dataBinding.msgTV.text = "No item found"
                    } else {
                        dataBinding.listRV.visibility = View.VISIBLE
                        dataBinding.msgTV.visibility = View.GONE

                        adapter = RecipientRequestMedicineAdapter(availableArray, this)
                        dataBinding.listRV.layoutManager = LinearLayoutManager(context)
                        dataBinding.listRV.isNestedScrollingEnabled = false
                        dataBinding.listRV.adapter = adapter
                    }
                    loadingView.dismiss()

                }.addOnFailureListener {
                    dataBinding.listRV.visibility = View.GONE
                    dataBinding.msgTV.visibility = View.VISIBLE
                    dataBinding.msgTV.text = "Something went wrong"

                    print(it.message)
                    loadingView.dismiss()
                }

        } else {
            displayToast("No internet connection")
        }

    }


    override fun onRequestMedicineSelect(recipientMedicine: RecipientMedicine, from: Int) {
        val request = myDB.collection("request")
        val medicineC = myDB.collection("medicine")

        if (from == 1) {
            request.document(recipientMedicine.id).update(mapOf("status" to "2"))
                .addOnSuccessListener {
                    displayToast("Accepted")
                    loadingView.dismiss()
                    getData()
                }.addOnFailureListener {
                    displayToast("Sorry! Something went wrong.")
                    loadingView.dismiss()
                }

        } else if(from == 2){

            request.document(recipientMedicine.id).update(mapOf("status" to "3"))
                .addOnSuccessListener {
                    medicineC.whereEqualTo("id",recipientMedicine.medicineId).limit(1)
                    medicineC.get().addOnSuccessListener {
                        val docSP = it.documents
                     for (doc in docSP){
                         if (doc.getString("id") == recipientMedicine.medicineId){
                             val av_qtn = doc.getString("av_qtn")?.toInt()
                             val name = doc.getString("medicineName")
                             Log.e("name "," $name")
                             val rQtn = recipientMedicine.qtn.toInt()
                             Log.e("avQtn "," $av_qtn")
                             Log.e("rQtn "," $rQtn")

                             val fQtn = av_qtn?.plus(rQtn)
                             Log.e("fQtn "," $fQtn")
                             medicineC.document(recipientMedicine.medicineId).update(mapOf("av_qtn" to fQtn.toString()))
                             displayToast("Rejected")
                             loadingView.dismiss()
                             getData()
                             break
                         }

                     }
                    }.addOnFailureListener{
                        displayToast("Sorry! Something went wrong.1")
                        loadingView.dismiss()
                        getData()
                    }
                }.addOnFailureListener {
                    displayToast("Sorry! Something went wrong.")
                    loadingView.dismiss()
                }

        }else if (from == 3){
            val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:${recipientMedicine.recipientNumber}"))
            startActivity(intent)
        }else{
            val smsIntent = Intent(Intent.ACTION_VIEW)
            smsIntent.type = "vnd.android-dir/mms-sms"
            smsIntent.putExtra("address", recipientMedicine.recipientNumber)
            smsIntent.putExtra(
                "sms_body",
                "Hello, Do you need the medicine of ${recipientMedicine.medicineName}.\n\n\n Name: ${
                    prefs.getValue(prefs.name)
                }\nAddress: ${prefs.getValue(prefs.areaName)}, ${prefs.getValue(prefs.cityName)}"
            )
            startActivity(smsIntent)
        }
    }
}