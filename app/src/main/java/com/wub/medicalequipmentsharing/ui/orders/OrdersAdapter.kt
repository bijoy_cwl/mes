package com.wub.medicalequipmentsharing.ui.orders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.wub.medicalequipmentsharing.R
import com.wub.medicalequipmentsharing.model.RecipientMedicine

class OrdersAdapter(
    private val from: Int,
    private var list: List<RecipientMedicine>,
    private val ordersListener: OrdersListener
) :
    RecyclerView.Adapter<OrdersAdapter.OrdersHolder>() {

    inner class OrdersHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val medicineNameTV: TextView = itemView.findViewById(R.id.medicineNameTV)
        val qtyTV: TextView = itemView.findViewById(R.id.qtyTV)
        val nameTV: TextView = itemView.findViewById(R.id.nameTV)
        val addressTV: TextView = itemView.findViewById(R.id.addressTV)
        val statusTV: TextView = itemView.findViewById(R.id.statusTV)
        val rejectButton: Button = itemView.findViewById(R.id.rejectButton)
//        val acceptButton: Button = itemView.findViewById(R.id.acceptButton)
    }

//    fun setFilter(iDayses: List<District>) {
//        list = ArrayList()
//        (list as ArrayList<District>).clear()
//        (list as ArrayList<District>).addAll(iDayses)
//        notifyDataSetChanged()
//    }

    public interface OrdersListener {
        fun onOrdersSelect(recipientMedicine: RecipientMedicine,from: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrdersHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.order_item_layout, parent, false);
        return OrdersHolder(view)
    }

    override fun onBindViewHolder(holder: OrdersHolder, position: Int) {
        val item = list[holder.adapterPosition]
        holder.medicineNameTV.text = "Medicine Name: ${item.medicineName}"
        holder.qtyTV.text = "Quantity: ${item.qtn}"
        holder.addressTV.text = "Address: ${item.address}"
        holder.nameTV.text = "Recipient Name: ${item.recipient}"
        holder.rejectButton.visibility = View.GONE
        if (item.status == "1"){
            holder.statusTV.text = "Pending"
        }else if (item.status == "2"){
            holder.statusTV.text = "Accepted"
        }else{
            holder.statusTV.text = "Rejected"
        }

//        holder.acceptButton.setOnClickListener {
//            ordersListener.onOrdersSelect(item,1)
//        }
        holder.rejectButton.setOnClickListener {
            ordersListener.onOrdersSelect(item,2)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}