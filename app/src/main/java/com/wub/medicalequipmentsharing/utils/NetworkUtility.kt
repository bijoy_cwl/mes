package com.wub.medicalequipmentsharing.utils

import android.content.Context
import android.net.ConnectivityManager
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

object NetworkUtility {

    fun isNetworkAvailable(context: Context): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = cm.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    fun isKnownException(t: Throwable): Boolean {
        return (t is ConnectException
                || t is UnknownHostException
                || t is SocketTimeoutException)
    }

}
