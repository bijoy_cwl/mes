package com.wub.medicalequipmentsharing.model

data class RecipientMedicine(
    val id: String,
    val medicineId: String,
    val medicineName: String,
    val qtn: String,
    val address: String,
    val recipient: String,
    val recipientNumber: String,
    val donorNumber: String,
    val status: String
)