package com.wub.medicalequipmentsharing.ui.signup

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.databinding.DataBindingUtil
import com.wub.medicalequipmentsharing.R
import com.wub.medicalequipmentsharing.databinding.ActivitySignUpBinding
import com.wub.medicalequipmentsharing.model.District
import com.wub.medicalequipmentsharing.model.Upazila
import com.wub.medicalequipmentsharing.model.UserInfo
import com.wub.medicalequipmentsharing.ui.BaseActivity
import com.wub.medicalequipmentsharing.ui.home.HomeActivity
import com.wub.medicalequipmentsharing.ui.signin.SignInActivity
import com.wub.medicalequipmentsharing.utils.Constants.isValidEmail
import com.wub.medicalequipmentsharing.utils.Constants.md5
import com.wub.medicalequipmentsharing.utils.NetworkUtility

class SignUpActivity : BaseActivity() {

    lateinit var dataBinding: ActivitySignUpBinding
    var district: District? = null
    var upazila: Upazila? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up)
        val users = myDB.collection("users")

        dataBinding.signInHereTV.setOnClickListener {
            startActivity(Intent(this, SignInActivity::class.java))
            finish()
        }

        dataBinding.backIV.setOnClickListener {
            startActivity(Intent(this, SignInActivity::class.java))
            finish()
        }

        val resultLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
                if (result.resultCode == 100) {
                    // There are no request codes
                    val data: Intent? = result.data
                    // doSomeOperations()
                    district = data?.getSerializableExtra("data") as District
                    dataBinding.cityTV.text = district!!.name
                    dataBinding.upazilaTitleTV.visibility = View.VISIBLE
                    dataBinding.upazilaLL.visibility = View.VISIBLE
                } else if (result.resultCode == 101) {
                    val data: Intent? = result.data
                    // doSomeOperations()
                    upazila = data?.getSerializableExtra("data") as Upazila
                    dataBinding.upaziliTV.text = upazila!!.name

                }
            }

        if (district == null) {
            dataBinding.cityTV.text = "Select a city"
            dataBinding.upazilaTitleTV.visibility = View.GONE
            dataBinding.upazilaLL.visibility = View.GONE
        }
        if (upazila == null) {
            dataBinding.upaziliTV.text = "Select an area"
        }

        dataBinding.cityTV.setOnClickListener {
            val intent = Intent(this, DistrictsSelectActivity::class.java)
            intent.putExtra("from", 1)
            intent.putExtra("disId", "")
            resultLauncher.launch(intent)
        }
        dataBinding.upazilaLL.setOnClickListener {
            val intent = Intent(this, DistrictsSelectActivity::class.java)
            intent.putExtra("from", 2)
            intent.putExtra("disId", district?.id)
            resultLauncher.launch(intent)
        }


        dataBinding.signUpButton.setOnClickListener {
            val name = dataBinding.nameET.text.toString().trim()
            val number = dataBinding.numberET.text.toString().trim()
            val email = dataBinding.emailET.text.toString().trim()
            val password = dataBinding.passwordET.text.toString().trim()

            when {
                name == "" -> {
                    displayToast("Enter your name")
                    dataBinding.nameET.requestFocus()
                }
                number == "" -> {
                    displayToast("Enter your number")
                    dataBinding.numberET.requestFocus()
                }
                email == "" || !isValidEmail(email) -> {
                    displayToast("Enter your vaild email")
                    dataBinding.emailET.requestFocus()
                }
                district == null -> {
                    displayToast("Select a city")
                }
                upazila == null -> {
                    displayToast("Select an area")
                }
                password == "" -> {
                    displayToast("Enter your password")
                    dataBinding.passwordET.requestFocus();
                }
                else -> {
                    if (NetworkUtility.isNetworkAvailable(context)) {
                        loadingView.show()
                        users.whereEqualTo("number", number).get().addOnCompleteListener {
                            if (it.result?.documents?.size!! > 0) {
                                loadingView.dismiss()
                                displayToast("User Exists")
                            } else {
                                users.document(number).set(
                                    mapOf(
                                        "name" to name,
                                        "number" to number,
                                        "email" to email,
                                        "city" to district!!.id,
                                        "cityName" to district!!.name,
                                        "area" to upazila!!.id,
                                        "areaName" to upazila!!.name,
                                        "password" to md5(password),
                                    )
                                ).addOnSuccessListener {

                                    val userInfo = UserInfo(
                                        name,
                                        number,
                                        email,
                                        district!!.id,
                                        district!!.name,
                                        upazila!!.id,
                                        upazila!!.name
                                    )
                                    prefs.setUser(userInfo, true)
                                    displayToast("Registration Successful")
                                    loadingView.dismiss()

                                    val intent = Intent(this, HomeActivity::class.java)
                                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                    startActivity(intent)
                                    finish()
                                }.addOnFailureListener {
                                    displayToast("Registration Failed")
                                    loadingView.dismiss()
                                }

                            }
                        }

                    } else {
                        displayToast("No Internet connection")
                    }

                }
            }

        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(this, SignInActivity::class.java))
        finish()
    }
}