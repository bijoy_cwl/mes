package com.wub.medicalequipmentsharing.ui.requestmedicine

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.wub.medicalequipmentsharing.R

import com.wub.medicalequipmentsharing.model.Medicine

class RequestMedicineAdapter(
    private var list: List<Medicine>,
    private val requestMedicineListener: RequestMedicineListener
) :
    RecyclerView.Adapter<RequestMedicineAdapter.RequestMedicineHolder>() {

    inner class RequestMedicineHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val medicineNameTV: TextView = itemView.findViewById(R.id.medicineNameTV)
        val qtyTV: TextView = itemView.findViewById(R.id.qtyTV)
        val nameTV: TextView = itemView.findViewById(R.id.nameTV)
        val addressTV: TextView = itemView.findViewById(R.id.addressTV)
        val requestButton: Button = itemView.findViewById(R.id.requestButton)
        val callButton: Button = itemView.findViewById(R.id.callButton)
        val msgButton: Button = itemView.findViewById(R.id.msgButton)
    }

//    fun setFilter(iDayses: List<District>) {
//        list = ArrayList()
//        (list as ArrayList<District>).clear()
//        (list as ArrayList<District>).addAll(iDayses)
//        notifyDataSetChanged()
//    }

    public interface RequestMedicineListener {
        fun onRequestMedicineSelect(medicine: Medicine,from: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RequestMedicineHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.medicine_item_layout, parent, false);
        return RequestMedicineHolder(view)
    }

    override fun onBindViewHolder(holder: RequestMedicineHolder, position: Int) {
        val item = list[holder.adapterPosition]
        holder.medicineNameTV.text = "Medicine Name: ${item.name}"
        holder.qtyTV.text = "Quantity: ${item.av_qtn}"
        holder.addressTV.text = "Address: ${item.address}"
        holder.nameTV.text = "Donor Name: ${item.donorName}"

        holder.requestButton.setOnClickListener {
            requestMedicineListener.onRequestMedicineSelect(item,3)
        }

        holder.callButton.setOnClickListener {
            requestMedicineListener.onRequestMedicineSelect(item,1)
        }

        holder.msgButton.setOnClickListener {
            requestMedicineListener.onRequestMedicineSelect(item,2)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }
}