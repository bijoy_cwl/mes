package com.wub.medicalequipmentsharing.model

class UserInfo(
    val name: String,
    val number: String,
    val email: String,
    val city: String,
    val cityName: String,
    val area: String,
    val areaName: String
)