package com.wub.medicalequipmentsharing.model

import java.io.Serializable

data class Upazila(val id: String, val district_id: String, val name: String): Serializable