package com.wub.medicalequipmentsharing.ui.aboutus

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.wub.medicalequipmentsharing.R
import com.wub.medicalequipmentsharing.databinding.ActivityAboutUsBinding
import com.wub.medicalequipmentsharing.databinding.ActivityHomeBinding

class AboutUsActivity : AppCompatActivity() {
    lateinit var dataBinding: ActivityAboutUsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_about_us)

        dataBinding.backIV.setOnClickListener {
            finish()
        }
    }
}