package com.wub.medicalequipmentsharing.utils

import android.content.Context
import android.util.Log
import android.util.Patterns
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.wub.medicalequipmentsharing.model.District
import com.wub.medicalequipmentsharing.model.Upazila
import java.io.IOException
import java.math.BigInteger
import java.security.MessageDigest
import java.util.regex.Pattern


object Constants {

    fun getDistricts(context: Context): List<District> {

        val jsonFileString = getJsonDataFromAsset(context, "districts.json")
        //   jsonFileString?.let { Log.i("data", it) }

        val gson = Gson()
        val listPersonType = object : TypeToken<List<District>>() {}.type

        val persons: List<District> = gson.fromJson(jsonFileString, listPersonType)
        return persons
    }

    fun getUpazilas(context: Context, districtId: String): List<Upazila> {

        val jsonFileString = getJsonDataFromAsset(context, "upazilas.json")

        val gson = Gson()
        val listPersonType = object : TypeToken<List<Upazila>>() {}.type

        val persons: List<Upazila> = gson.fromJson(jsonFileString, listPersonType)
        return persons.filter { it.district_id == districtId }
    }

    fun getDistrictsById(context: Context, districtId: String): District {

        val jsonFileString = getJsonDataFromAsset(context, "districts.json")
        //   jsonFileString?.let { Log.i("data", it) }

        val gson = Gson()
        val listPersonType = object : TypeToken<List<District>>() {}.type

        val persons: List<District> = gson.fromJson(jsonFileString, listPersonType)

        return persons.filter { it.id == districtId}[0]
    }

    fun getUpazilasById(context: Context, upazilaId: String): Upazila {

        val jsonFileString = getJsonDataFromAsset(context, "upazilas.json")

        val gson = Gson()
        val listPersonType = object : TypeToken<List<Upazila>>() {}.type

        val persons: List<Upazila> = gson.fromJson(jsonFileString, listPersonType)

        return persons.filter { it.id == upazilaId }[0]
    }

    fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }

     fun isValidEmail(email: String): Boolean {
        val pattern: Pattern = Patterns.EMAIL_ADDRESS
        return pattern.matcher(email).matches()
    }

    fun md5(input:String): String {
        val md = MessageDigest.getInstance("MD5")
        return BigInteger(1, md.digest(input.toByteArray())).toString(16).padStart(32, '0')
    }
}