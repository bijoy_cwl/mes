package com.wub.medicalequipmentsharing.ui

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore
import com.wub.medicalequipmentsharing.utils.MESPreference
import com.wub.medicalequipmentsharing.view.LoadingDialog


abstract class BaseActivity : AppCompatActivity() {
    val context: Context = this
    lateinit var loadingView: LoadingDialog
    var myDB = FirebaseFirestore.getInstance()
    lateinit var prefs: MESPreference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadingView = LoadingDialog(this)
        prefs = MESPreference(context)

    }

    fun displayToast(msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
    }
}