package com.wub.medicalequipmentsharing.ui.requestmedicine

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.databinding.DataBindingUtil
import com.wub.medicalequipmentsharing.R
import com.wub.medicalequipmentsharing.databinding.ActivityFilterBinding
import com.wub.medicalequipmentsharing.databinding.ActivityRequestMedicineBinding
import com.wub.medicalequipmentsharing.model.District
import com.wub.medicalequipmentsharing.model.FilterData
import com.wub.medicalequipmentsharing.model.Upazila
import com.wub.medicalequipmentsharing.ui.BaseActivity
import com.wub.medicalequipmentsharing.ui.signup.DistrictsSelectActivity
import com.wub.medicalequipmentsharing.utils.Constants

class FilterActivity : BaseActivity() {

    lateinit var dataBinding: ActivityFilterBinding
    var district: District? = null
    var upazila: Upazila? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_filter)
        dataBinding.backIV.setOnClickListener {
            finish()
        }
        val cityId = prefs.getValue(prefs.city)
        val areaId = prefs.getValue(prefs.area)
        if (cityId != "") {
            district = Constants.getDistrictsById(applicationContext, cityId)
        }
        if (areaId != "") {
            upazila = Constants.getUpazilasById(applicationContext, areaId)
        }


        val resultLauncher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
                if (result.resultCode == 100) {
                    // There are no request codes
                    val data: Intent? = result.data
                    // doSomeOperations()
                    val district1 = data?.getSerializableExtra("data") as District
                    if (district != null && (district!!.id != district1.id)) {
                        upazila = null
                        dataBinding.upaziliTV.text = "Select an area"
                        dataBinding.upazilaTitleTV.visibility = View.VISIBLE
                        dataBinding.upazilaLL.visibility = View.VISIBLE
                    } else {
                        dataBinding.upazilaTitleTV.visibility = View.VISIBLE
                        dataBinding.upazilaLL.visibility = View.VISIBLE
                    }
                    district = district1
                    dataBinding.cityTV.text = district!!.name

                } else if (result.resultCode == 101) {
                    val data: Intent? = result.data
                    // doSomeOperations()
                    upazila = data?.getSerializableExtra("data") as Upazila
                    dataBinding.upaziliTV.text = upazila!!.name

                }
            }

        if (district == null) {
            dataBinding.cityTV.text = "Select a city"
            dataBinding.upazilaTitleTV.visibility = View.GONE
            dataBinding.upazilaLL.visibility = View.GONE
        } else {
            dataBinding.cityTV.text = district?.name
        }
        if (upazila == null) {
            dataBinding.upaziliTV.text = "Select an area"
        } else {
            dataBinding.upaziliTV.text = upazila!!.name
            dataBinding.upazilaTitleTV.visibility = View.VISIBLE
            dataBinding.upazilaLL.visibility = View.VISIBLE
        }

        dataBinding.cityTV.setOnClickListener {
            val intent = Intent(this, DistrictsSelectActivity::class.java)
            intent.putExtra("from", 1)
            intent.putExtra("disId", "")
            resultLauncher.launch(intent)
        }
        dataBinding.upazilaLL.setOnClickListener {
            val intent = Intent(this, DistrictsSelectActivity::class.java)
            intent.putExtra("from", 2)
            intent.putExtra("disId", district?.id)
            resultLauncher.launch(intent)
        }

        dataBinding.cancelButton.setOnClickListener {
            finish()
        }
        dataBinding.submitButton.setOnClickListener {
            val intent = Intent();
            val fData = district?.let { it1 -> upazila?.let { it2 -> FilterData(it1, it2) } }
            intent.putExtra("data", fData)
            setResult(105, intent);
            finish();
        }


    }


}